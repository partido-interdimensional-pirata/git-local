# © 2017 fauno <fauno@partidopirata.com.ar>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
# 
# http://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html
#

.DEFAULT_GOAL := help
project_dir := /home/$(USER)/.projects
service_dir := /home/$(USER)/.config/systemd/user
dirs := $(project_dir) $(service_dir)
service := $(service_dir)/git-daemon.service
repo_dir := $(project_dir)/$(repo).git

systemctl := systemctl --user
torrc     := /etc/tor/torrc
hostname  := $(shell hostname)
tor_hostname := /var/lib/tor/$(hostname)/hostname
# No lleva : para poder generarla cuando sea necesario
onion      = $(shell cat $(tor_hostname))

# Asegurarse que las variables llegan a las plantillas
export

help: ## Ayuda
	@grep -E "^\w+:" $(MAKEFILE_LIST) \
		| sed -re "s/^(\w+):.*## (.*)/\1;\2/" \
		| column -t -s ";"
dirs: $(dirs) ## Crear todos los directorios
service: $(service) ## Instalar el servicio
all: dirs service ## Instalar todo
	$(systemctl) enable $(notdir $(service))
	$(systemctl) start  $(notdir $(service))
uninstall: ## Desinstalar todo
	$(systemctl) stop    $(notdir $(service)) ; true
	$(systemctl) disable $(notdir $(service)) ; true
	rm -rfvI $(service) $(project_dir) ; true
init: test-repo $(repo_dir) info-repo ## Inicia un repositorio: make init repo=nombre_del_repositorio
tor: $(torrc) ## Configura Tor
	systemctl restart tor
	while ! test -f $(tor_hostname); do sleep 1s; done; true
	@echo "Tus repositorios están publicados en Tor:"
	@echo "git://$(onion)/nombre_del_repositorio.git"

# Plantillas!
%: %.in
	envsubst < $< > $@

# Instalar directorios
$(dirs):
	install -dm2750 -o $(USER) -g $(USER) $@

$(service): git-daemon.service
	install -m 640 -o $(USER) -g $(USER) $< $@
	rm -f $<

$(repo_dir):
	test -d $(repo_dir) || git init --bare $(repo_dir)

test-repo:
	@test -z "$(repo)" && echo "Uso: make init repo=nombre_del_repositorio" && exit 1 ; :

info-repo:
	@echo "Ahora podés agregar este repo en tu repositorio local:"
	@echo "  git remote add local $(repo_dir)"

$(torrc): torrc
	grep -q "local-git" $@ || cat $< >> $@
	rm -f $<
