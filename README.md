# Alojar repositorios git localmente

Los repositorios git se alojan localmente, en nuestra computadoras.
Luego se pueden clonar y pullear (no pushear) usando una dirección
local, como la dirección IP en la LAN, el `hostname`, el
`hostname.local` si usamos mDNS (avahi), la IP en [LibreVPN][] o la
dirección .onion de Tor.

[LibreVPN]: https://github.com/LibreVPN/librevpn

## Instalar

```bash
# Crea los directorios, instala y habilita el daemon de git
make all
# A partir de este momento, se lo puede controlar con
systemctl --user stop git-daemon.service
```

## Alojar un repositorio

Los repositorios van a estar alojados en `~/.projects`.

```bash
	make init repo=nombre_del_repositorio
```

TODO: Convertir en un comando de git, estilo `git init-local-repo` o algo así.

## Clonar o pullear

```bash
git clone git://hostname_o_direccion_ip/nombre_del_repositorio.git
```

## Pushear

No hay `push` remoto, los repositorios son de solo lectura.

El `push` local se realiza agregando el repositorio en los `remotes`, como
indica el `make init`.

## Configurar Tor

Para poder publicar los repositorios sobre Tor, hay que configurar el
_hidden service_.

```bash
sudo make tor
```

### Clonar sobre Tor

```bash
torsocks git clone git://hostname_de_tor.onion/nombre_del_repositorio.git
```
